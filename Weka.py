import weka.core.jvm as jvm
from weka.core.converters import Loader
from weka.classifiers import Classifier, Evaluation, Kernel, KernelClassifier
from weka.core.classes import Random
import mysql.connector as mysql
import time

#Test set
test_set = "M"

#Test number
test_num = 10

#base filename
base_filename = "merged_5s_100_80_binary_scaled"

#library name
library = "weka"

#Algorithm configurations
MLP_config = "sigmoid, sgd, (16,8)"

KNN_config = "kdtree"

SVM_config = "rbf"

#Algorithm notes
MLP_notes = "Shogun and Scikit-learn L-BFGS for MLP, Weka SGD"

KNN_notes = "-"

SVM_notes = ""

#Start JVM
jvm.start(packages=True)

for i in range(0,3):

    #import train and test data
    loader = Loader(classname="weka.core.converters.CSVLoader", options=["-H", "-N", "last"])
    train_data = loader.load_file("Data/Processed/" + base_filename + "_train.csv")
    test_data = loader.load_file("Data/Processed/" + base_filename + "_test.csv")
    train_data.class_is_last()
    test_data.class_is_last()


    MLP = Classifier(classname="weka.classifiers.functions.MultilayerPerceptron", 
        options=["-I", "-C", "-H", "16,8", "-N", "200", "-M", "0.9", "-L", "0.001"])
    MLP.batch_size = "10"
    
    KNN = Classifier(classname="weka.classifiers.lazy.IBk", 
        options=["-A", "weka.core.neighboursearch.KDTree"])

    SVM = Classifier(classname="weka.classifiers.functions.LibSVM", options=["-K", "2"])

    print(train_data.summary(train_data))
    print(test_data.summary(test_data))

    start = time.clock()
    MLP.build_classifier(train_data)
    end = time.clock()
    MLP_train_time = end - start

    start = time.clock()
    KNN.build_classifier(train_data)
    end = time.clock()
    KNN_train_time = end - start

    start = time.clock()
    SVM.build_classifier(train_data)
    end = time.clock()
    SVM_train_time = end - start

    #Classify and measure times

    MLP_eval = Evaluation(test_data)
    KNN_eval = Evaluation(test_data)
    SVM_eval = Evaluation(test_data)

    start = time.clock()
    MLP_eval.test_model(MLP, test_data)
    end = time.clock()
    MLP_predict_time = end - start

    start = time.clock()
    KNN_eval.test_model(KNN, test_data)
    end = time.clock()
    KNN_predict_time = end - start

    start = time.clock()
    SVM_eval.test_model(SVM, test_data)
    end = time.clock()
    SVM_predict_time = end - start

    MLP_tn, MLP_fp, MLP_fn, MLP_tp = MLP_eval.confusion_matrix.ravel()
    KNN_tn, KNN_fp, KNN_fn, KNN_tp = KNN_eval.confusion_matrix.ravel()
    SVM_tn, SVM_fp, SVM_fn, SVM_tp = SVM_eval.confusion_matrix.ravel()

    #Create database entries
    MLP_fields = {
    'test_set': test_set,
    'test_number': test_num,
    'base_filename': base_filename,
    'library': library,
    'algorithm': "MLP",
    'config': MLP_config,
    'train_time': str(MLP_train_time),
    'predict_time': str(MLP_predict_time),
    'true_positives': str(int(MLP_tp)),
    'true_negatives': str(int(MLP_tn)),
    'false_positives': str(int(MLP_fp)),
    'false_negatives': str(int(MLP_fn)),
    'notes': MLP_notes
    }

    KNN_fields = {
    'test_set': test_set,
    'test_number': test_num,
    'base_filename': base_filename,
    'library': library,
    'algorithm': "KNN",
    'config': KNN_config,
    'train_time': str(KNN_train_time),
    'predict_time': str(KNN_predict_time),
    'true_positives': str(int(KNN_tp)),
    'true_negatives': str(int(KNN_tn)),
    'false_positives': str(int(KNN_fp)),
    'false_negatives': str(int(KNN_fn)),
    'notes': KNN_notes
    }

    SVM_fields = {
    'test_set': test_set,
    'test_number': test_num,
    'base_filename': base_filename,
    'library': library,
    'algorithm': "SVM",
    'config': SVM_config,
    'train_time': str(SVM_train_time),
    'predict_time': str(SVM_predict_time),
    'true_positives': str(int(SVM_tp)),
    'true_negatives': str(int(SVM_tn)),
    'false_positives': str(int(SVM_fp)),
    'false_negatives': str(int(SVM_fn)),
    'notes': SVM_notes
    }

    insert_statement= ("INSERT INTO test_results "
                   "(test_set, test_number, base_filename, library, algorithm, config, train_time, predict_time, "
                   "true_positives, true_negatives, false_positives, false_negatives, notes) VALUES "
                   "(%(test_set)s, %(test_number)s, %(base_filename)s, %(library)s, %(algorithm)s, %(config)s, "
                   "%(train_time)s, %(predict_time)s, %(true_positives)s, %(true_negatives)s, "
                   "%(false_positives)s, %(false_negatives)s, %(notes)s)")

    #create connection and cursor
    connection = mysql.connect(user='root', password='', host='127.0.0.1', database='mltests')
    cursor = connection.cursor()

    #execute commands
    cursor.execute(insert_statement, MLP_fields)
    cursor.execute(insert_statement, KNN_fields)
    cursor.execute(insert_statement, SVM_fields)

    #commit changes
    connection.commit()

    #close connection
    cursor.close()
    connection.close()

    print("MLP TN: ", MLP_tn)
    print("MLP FP: ", MLP_fp) 
    print("MLP FN", MLP_fn) 
    print("MLP TP", MLP_tp) 
    print("MLP Accuracy: ", (MLP_tp+MLP_tn)/(MLP_tp+MLP_tn+MLP_fp+MLP_fn))
    print("KNN TN: ", KNN_tn)
    print("KNN FP: ", KNN_fp) 
    print("KNN FN", KNN_fn) 
    print("KNN TP", KNN_tp) 
    print("KNN Accuracy: ", (KNN_tp+KNN_tn)/(KNN_tp+KNN_tn+KNN_fp+KNN_fn))
    print("SVM TN: ", SVM_tn)
    print("SVM FP: ", SVM_fp) 
    print("SVM FN", KNN_fn) 
    print("SVM TP", KNN_tp) 
    print("SVM Accuracy: ", (SVM_tp+SVM_tn)/(SVM_tp+SVM_tn+SVM_fp+SVM_fn))

