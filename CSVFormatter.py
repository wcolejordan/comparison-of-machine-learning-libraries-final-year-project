# This script takes an input CSV, converting 'NONTOR' and 'TOR' labels to numerical values and strips column names.
# It then proceeds to scale the features to ensure scaling is uniform across experiments

#required import
import pandas as pd
import numpy as np
from sklearn import preprocessing, model_selection
import mysql.connector as mysql


# Inputs, adjust settings as required

#input path
input_path = "Data/Original/Scenario-A/SelectedFeatures-10s-TOR-NonTOR.csv"
input_path = "Data/Original/Scenario-A/merged_5s.csv"
#output base filename
base_name = "SelectedFeatures_10s-TOR-NonTor_100_80_binary_scaled"

#output path
output_path = "Data/Processed/"

#file subset (1=100%)
subset_val = 1

#train/test split (1=100%)
split_val = 0.8

#scale features
scaled = True

#seed for randomization
random_seed = 1

#notes
notes = ""

#remove first cols (5 adequate)
remove_first_cols = 5


# Process input file

#Convert list input to key value pairs
def listtodict(listinput):
    out = {}
    for i in listinput:
        out[i] = 0 if listinput.index(i) == 0 else 1
    return out

#formats output path to same directory
train_name = base_name + "_train.csv"
test_name = base_name + "_test.csv"
train_output =  output_path + train_name
test_output = output_path + test_name

#Read data from CSV
df = pd.read_csv(input_path, header=0, low_memory=False)
df.replace(["Infinity"], np.nan, inplace=True)
df.fillna(value=0, inplace=True)


#Get unique values from labels column and convert to dict in format {'NONTOR': 1, 'TOR': 2}
label = listtodict(df["label"].unique().tolist())

#Replace strings to numbers in dataframe NONTOR=1, TOR=2
df.loc[df["label"].isin(label.keys()), 'label'] = df["label"].map(label)

#Remove first columns
columns = [x for x in range(remove_first_cols)]
df.drop(df.columns[columns], axis=1, inplace=True)
shuffled = df.sample(frac=1, random_state=1)
shuffled.astype('float64')

#trim set
subset_number = int(len(shuffled) * subset_val)
subset = shuffled.iloc[:subset_number]

#split to train and test
split_number = int(len(subset) * split_val)
train_df = subset.iloc[:split_number]
test_df = subset.iloc[split_number:]


# Output files and log to database

if(scaled):
    #Split features and labels
    X_train = np.array(train_df.drop(train_df.columns[-1], 1).astype(np.float))
    X_test = np.array(test_df.drop(test_df.columns[-1], 1).astype(np.float))
    
    y_train = np.array(train_df[train_df.columns[-1]].astype(np.float))
    y_test = np.array(test_df[test_df.columns[-1]].astype(np.float))

    #scale X [features]
    X_train = preprocessing.scale(X_train)
    X_test = preprocessing.scale(X_test)

    #join scaled X values with y
    data_scaled_train = np.concatenate((X_train,np.matrix(y_train).T), axis=1)
    data_scaled_test = np.concatenate((X_test,np.matrix(y_test).T), axis=1)
    
    np.savetxt(train_output, data_scaled_train, delimiter=',')
    np.savetxt(test_output, data_scaled_test, delimiter=',')
    
else:
    train_df.to_csv(train_output, header=False, index=False)
    test_df.to_csv(test_output, header=False, index=False)
    
#Database fields to be filled
fields = {
    'base_filename': base_name,
    'train_path': train_output,
    'test_path': test_output,
    'original_file': input_path,
    'train_count': len(train_df),
    'test_count': len(test_df),
    'scaled': scaled,
    'random_seed': random_seed,
    'first_cols_removed': remove_first_cols,
    'notes': notes
}

insert_statement = ("INSERT INTO file_tracker "
                    "(base_filename, train_path, test_path, original_file, train_count, test_count, "
                    "scaled, random_seed, first_cols_removed, notes)"
                    "VALUES (%(base_filename)s, %(train_path)s, %(test_path)s, %(original_file)s, "
                    "%(train_count)s, %(test_count)s, %(scaled)s, %(random_seed)s, "
                    "%(first_cols_removed)s, %(notes)s)")

#create connection and cursor
connection = mysql.connect(user='root', password='', host='127.0.0.1', database='mltests')
cursor = connection.cursor()

#execute commands
cursor.execute(insert_statement, fields)

#commit changes
connection.commit()

#close connection
cursor.close()
connection.close()



