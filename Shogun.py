from shogun import RealFeatures, BinaryLabels, DynamicObjectArray
from shogun import NeuralNetwork, NeuralInputLayer, NeuralLogisticLayer, NeuralSoftmaxLayer, AccuracyMeasure
from shogun import LibSVM, GaussianKernel, L2R_L2LOSS_SVC
import shogun as sg
from sklearn import model_selection

import os
import pandas as pd
import numpy as np
import time
import mysql.connector as mysql

#Test set
test_set = "M"

#Test number
test_num = 10

#base filename
base_filename = "merged_5s_100_80_binary_scaled"

#library name
library = "shogun"

#Algorithm configurations
MLP_config = "sigmoid, lbfgs, (16,8)"

KNN_config = "kdtree"

SVM_config = "-"

#Algorithm notes
MLP_notes = "Shogun and Scikit-learn L-BFGS for MLP, Weka SGD"

KNN_notes = "-"

SVM_notes = "-"

for i in range(0,3):
    #import train and test data
    train_df = pd.read_csv("Data/Processed/" + base_filename + "_train.csv", header=None, low_memory=False)
    test_df =  pd.read_csv("Data/Processed/" + base_filename + "_test.csv", header=None, low_memory=False)

    #parse X, y
    X_train = np.array(train_df.drop(train_df.columns[-1], 1))
    y_train = np.array(train_df[train_df.columns[-1]])
    X_test = np.array(test_df.drop(test_df.columns[-1], 1))
    y_test = np.array(test_df[test_df.columns[-1]])

    #Check shapes
    print("X_train.shape:", X_train.shape)
    print("y_train.shape:", y_train.shape)
    print()
    print("X_test.shape:", X_test.shape)
    print("y_test.shape:", y_test.shape)

    #Accuracy Measures
    acc = sg.MulticlassAccuracy()

    #Setup X
    X_train = RealFeatures(X_train.T)
    X_test = RealFeatures(X_test.T)

    #Setup y
    y_train[y_train == 2] = 0
    y_test[y_test == 2] = 0
    y_train = sg.MulticlassLabels(y_train.T)
    y_test = sg.MulticlassLabels(y_test.T)


    #Configure Algorithms

    #MLP
    num_feats = X_train.get_num_features()
    layers = DynamicObjectArray()
    layers.append_element(NeuralInputLayer(num_feats))
    layers.append_element(NeuralLogisticLayer(16))
    layers.append_element(NeuralLogisticLayer(8))
    layers.append_element(NeuralSoftmaxLayer(2))
    MLP = NeuralNetwork(layers)
    #MLP.set_optimization_method(sg.NNOM_GRADIENT_DESCENT)
    #MLP.set_gd_momentum(0.9)
    #MLP.set_gd_learning_rate(0.001)
    #MLP.set_gd_mini_batch_size(100)
    MLP.set_l2_coefficient(1e-4)
    MLP.set_epsilon(1e-8)
    MLP.set_max_num_epochs(200)
    MLP.quick_connect()
    MLP.initialize_neural_network()
    MLP.set_labels(y_train)

    #KNN
    k = 2
    dist = sg.EuclideanDistance()
    KNN = sg.KNN(k, dist, y_train, knn_solver = sg.KNN_KDTREE)

    #SVM
    C=1
    epsilon = 1e-3
    Gaussian_X_train = GaussianKernel(X_train, X_train, 15)
    SVM = sg.MulticlassLibSVM(C, Gaussian_X_train, y_train)
    SVM.set_epsilon(epsilon)

    #Train and measure times
    start = time.clock()
    MLP.train(X_train)
    end = time.clock()
    MLP_train_time = end - start

    start = time.clock()
    KNN.train(X_train)
    end = time.clock()
    KNN_train_time = end - start

    start = time.clock()
    SVM.train()
    end = time.clock()
    SVM_train_time = end - start

    #Classify and measure times
    start = time.clock()
    y_pred_MLP = MLP.apply_multiclass(X_test)
    end = time.clock()
    MLP_predict_time = end - start

    start = time.clock()
    y_pred_KNN = KNN.apply_multiclass(X_test)
    end = time.clock()
    KNN_predict_time = end - start

    start = time.clock()
    y_pred_SVM = SVM.apply_multiclass(X_test)
    end = time.clock()
    SVM_predict_time = end - start

    #Measure
    conf_mat_MLP = acc.get_confusion_matrix(y_pred_MLP, y_test)
    conf_mat_KNN = acc.get_confusion_matrix(y_pred_KNN, y_test)
    conf_mat_SVM = acc.get_confusion_matrix(y_pred_SVM, y_test)

    MLP_tn, MLP_fp, MLP_fn, MLP_tp = conf_mat_MLP.ravel()
    KNN_tn, KNN_fp, KNN_fn, KNN_tp = conf_mat_KNN.ravel()
    SVM_tn, SVM_fp, SVM_fn, SVM_tp = conf_mat_SVM.ravel()

    #Create database entries
    MLP_fields = {
    'test_set': test_set,
    'test_number': test_num,
    'base_filename': base_filename,
    'library': library,
    'algorithm': "MLP",
    'config': MLP_config,
    'train_time': str(MLP_train_time),
    'predict_time': str(MLP_predict_time),
    'true_positives': str(MLP_tp),
    'true_negatives': str(MLP_tn),
    'false_positives': str(MLP_fp),
    'false_negatives': str(MLP_fn),
    'notes': MLP_notes
    }

    KNN_fields = {
    'test_set': test_set,
    'test_number': test_num,
    'base_filename': base_filename,
    'library': library,
    'algorithm': "KNN",
    'config': KNN_config,
    'train_time': str(KNN_train_time),
    'predict_time': str(KNN_predict_time),
    'true_positives': str(KNN_tp),
    'true_negatives': str(KNN_tn),
    'false_positives': str(KNN_fp),
    'false_negatives': str(KNN_fn),
    'notes': KNN_notes
    }

    SVM_fields = {
    'test_set': test_set,
    'test_number': test_num,
    'base_filename': base_filename,
    'library': library,
    'algorithm': "SVM",
    'config': SVM_config,
    'train_time': str(SVM_train_time),
    'predict_time': str(SVM_predict_time),
    'true_positives': str(SVM_tp),
    'true_negatives': str(SVM_tn),
    'false_positives': str(SVM_fp),
    'false_negatives': str(SVM_fn),
    'notes': SVM_notes
    }

    insert_statement= ("INSERT INTO test_results "
                   "(test_set, test_number, base_filename, library, algorithm, config, train_time, predict_time, "
                   "true_positives, true_negatives, false_positives, false_negatives, notes) VALUES "
                   "(%(test_set)s, %(test_number)s, %(base_filename)s, %(library)s, %(algorithm)s, %(config)s, "
                   "%(train_time)s, %(predict_time)s, %(true_positives)s, %(true_negatives)s, "
                   "%(false_positives)s, %(false_negatives)s, %(notes)s)")

    #create connection and cursor
    connection = mysql.connect(user='root', password='', host='127.0.0.1', database='mltests')
    cursor = connection.cursor()

    #execute commands
    cursor.execute(insert_statement, MLP_fields)
    cursor.execute(insert_statement, KNN_fields)
    cursor.execute(insert_statement, SVM_fields)

    #commit changes
    connection.commit()

    #close connection
    cursor.close()
    connection.close()

    accuracy_MLP = acc.evaluate(y_pred_MLP, y_test)
    accuracy_KNN = acc.evaluate(y_pred_KNN, y_test)
    accuracy_SVM = acc.evaluate(y_pred_SVM, y_test)

    print("Multilayer Perceptron")
    print("Accuracy Score: ", accuracy_MLP)
    print("Train Time: " , str(MLP_train_time))
    print("Predict Time: " , str(MLP_predict_time))
    print("Confusion Matrix: \n", conf_mat_MLP)
    print()
    print("K-Nearest Neighbour")
    print("Accuracy Score: ", accuracy_KNN)
    print("Train Time: " , str(KNN_train_time))
    print("Predict Time: " , str(KNN_predict_time))
    print("Confusion Matrix: \n", conf_mat_KNN)
    print()
    print("Support Vector Machine")
    print("Accuracy: ", accuracy_SVM)
    print("Train Time: " , str(SVM_train_time))
    print("Predict Time: " , str(SVM_predict_time))
    print("Confusion Matrix: \n", conf_mat_SVM)

