from matplotlib import style
import matplotlib.pyplot as plt
get_ipython().magic('matplotlib inline')
import os
import matplotlib.patches as patches
import pandas as pd
import numpy as np
import time
import mysql.connector as mysql

select = "SELECT * FROM test_results WHERE test_set='M';"

#create connection and cursor
connection = mysql.connect(user='root', password='', host='127.0.0.1', database='mltests')
df = pd.read_sql(select, connection)
connection.close()

df['accuracy'] = df.apply(lambda row: ((row.true_positives + row.true_negatives)/(row.true_positives + row.false_positives + row.true_negatives + row.false_negatives)), axis=1)

df.head()

knn_df, mlp_df, svm_df = [x for _, x in df.groupby(df['algorithm'])]

knn_df = knn_df.groupby(['test_number', 'library', 'algorithm'])[['train_time', 'predict_time', 'accuracy']].mean()
mlp_df = mlp_df.groupby(['test_number', 'library', 'algorithm'])[['train_time', 'predict_time', 'accuracy']].mean()
svm_df = svm_df.groupby(['test_number', 'library', 'algorithm'])[['train_time', 'predict_time', 'accuracy']].mean()

knn_df.reset_index(inplace=True)
mlp_df.reset_index(inplace=True)
svm_df.reset_index(inplace=True)

knn_df.head()

fig, ax = plt.subplots(figsize=(8,6))
for label, sub_df in mlp_df.groupby(['library']):
    plt.plot(sub_df.test_number, sub_df.train_time, label=label)
    plt.xticks(sub_df.test_number)
plt.legend()
plt.grid()
plt.ylabel("Time (seconds)")
plt.xlabel("Test number")
plt.title("MLP Train Time")

fig, ax = plt.subplots(figsize=(8,6))
for label, sub_df in mlp_df.groupby(['library']):
    plt.plot(sub_df.test_number, sub_df.predict_time, label=label)
    plt.xticks(sub_df.test_number)
plt.legend()
plt.grid()
plt.ylabel("Time (seconds)")
plt.xlabel("Test number")
plt.title("MLP Predict Time")

fig, ax = plt.subplots(figsize=(8,6))
for label, sub_df in mlp_df.groupby(['library']):
    plt.plot(sub_df.test_number, sub_df.accuracy, label=label)
    plt.xticks(sub_df.test_number)
plt.legend()
plt.ylim([0.75,1])
plt.grid()
plt.ylabel("Accuracy")
plt.xlabel("Test number")
plt.title("MLP Accuracy")


fig, ax = plt.subplots(figsize=(8,6))
for label, sub_df in knn_df.groupby(['library']):
    plt.plot(sub_df.test_number, sub_df.train_time, label=label)
    plt.xticks(sub_df.test_number)
plt.legend()
plt.grid()
plt.ylabel("Time (seconds)")
plt.xlabel("Test number")
plt.title("KNN Train Time")

fig, ax = plt.subplots(figsize=(8,6))
for label, sub_df in knn_df.groupby(['library']):
    plt.plot(sub_df.test_number, sub_df.predict_time, label=label)
    plt.xticks(sub_df.test_number)
plt.legend()
plt.grid()
plt.ylabel("Time (seconds)")
plt.xlabel("Test number")
plt.title("KNN Predict Time")

fig, ax = plt.subplots(figsize=(8,6))
for label, sub_df in knn_df.groupby(['library']):
    plt.plot(sub_df.test_number, sub_df.accuracy, label=label)
    plt.xticks(sub_df.test_number)
plt.legend()
plt.ylim([0.75,1])
plt.grid()
plt.ylabel("Accuracy")
plt.xlabel("Test number")
plt.title("KNN Accuracy")


fig, ax = plt.subplots(figsize=(8,6))
for label, sub_df in svm_df.groupby(['library']):
    plt.plot(sub_df.test_number, sub_df.train_time, label=label)
    plt.xticks(sub_df.test_number)
plt.legend()
plt.grid()
plt.ylabel("Time (seconds)")
plt.xlabel("Test number")
plt.title("SVM Train Time")

fig, ax = plt.subplots(figsize=(8,6))
for label, sub_df in svm_df.groupby(['library']):
    plt.plot(sub_df.test_number, sub_df.predict_time, label=label)
    plt.xticks(sub_df.test_number)
plt.legend()
plt.grid()
plt.ylabel("Time (seconds)")
plt.xlabel("Test number")
plt.title("SVM Predict Time")

fig, ax = plt.subplots(figsize=(8,6))
for label, sub_df in svm_df.groupby(['library']):
    plt.plot(sub_df.test_number, sub_df.accuracy, label=label)
    plt.xticks(sub_df.test_number)
plt.legend()
plt.ylim([0.75,1])
plt.grid()
plt.ylabel("Accuracy")
plt.xlabel("Test number")
plt.title("SVM Accuracy")

