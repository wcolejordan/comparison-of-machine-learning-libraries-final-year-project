#Algorithms
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm

#Calculation and storage
from sklearn import metrics
import mysql.connector as mysql
import pandas as pd
import numpy as np
import time


# In[2]:


#Test set
test_set = "X"

#Test number
test_num = 1

#base filename
base_filename = "merged_5s_100_80_binary_scaled"

#library name
library = "scikit-learn"

#Algorithm configurations
MLP_config = "sigmoid, lbfgs"

KNN_config = "kdtree"

SVM_config = "rbf"

#Algorithm notes
MLP_notes = "Shogun and Scikit-learn L-BFGS for MLP, Weka SGD"

KNN_notes = "-"

SVM_notes = "-"


for i in range(0,3):
    #import train and test data
    train_df = pd.read_csv("Data/Processed/" + base_filename + "_train.csv", header=None, low_memory=False)
    test_df =  pd.read_csv("Data/Processed/" + base_filename + "_test.csv", header=None, low_memory=False)

    #parse X, y
    X_train = np.array(train_df.drop(train_df.columns[-1], 1))
    y_train = np.array(train_df[train_df.columns[-1]])
    X_test = np.array(test_df.drop(test_df.columns[-1], 1))
    y_test = np.array(test_df[test_df.columns[-1]])

    #Check shapes
    print("X_train.shape:", X_train.shape)
    print("y_train.shape:", y_train.shape)
    print()
    print("X_test.shape:", X_test.shape)
    
    print("y_test.shape:", y_test.shape)

    #Instantiate algorithms
    MLP = MLPClassifier(solver='lbfgs', activation='logistic', 
        alpha=1e-5, hidden_layer_sizes=(16,8))
    KNN = KNeighborsClassifier(algorithm='kd_tree')
    SVM = svm.SVC()

    #Train and measure times
    start = time.clock()
    MLP.fit(X_train, y_train)
    end = time.clock()
    MLP_train_time = end - start

    start = time.clock()
    KNN.fit(X_train, y_train)
    end = time.clock()
    KNN_train_time = end - start

    start = time.clock()
    SVM.fit(X_train, y_train)
    end = time.clock()
    SVM_train_time = end - start

    #Classify and measure times
    start = time.clock()
    y_pred_MLP = MLP.predict(X_test)
    end = time.clock()
    MLP_predict_time = end - start

    start = time.clock()
    y_pred_KNN = KNN.predict(X_test)
    end = time.clock()
    KNN_predict_time = end - start

    start = time.clock()
    y_pred_SVM = SVM.predict(X_test)
    end = time.clock()
    SVM_predict_time = end - start

    #extract ratios
    MLP_tn, MLP_fp, MLP_fn, MLP_tp = metrics.confusion_matrix(y_test, y_pred_MLP).ravel()
    KNN_tn, KNN_fp, KNN_fn, KNN_tp = metrics.confusion_matrix(y_test, y_pred_KNN).ravel()
    SVM_tn, SVM_fp, SVM_fn, SVM_tp = metrics.confusion_matrix(y_test, y_pred_SVM).ravel()

    #Create database entries
    MLP_fields = {
        'test_set': test_set,
        'test_number': test_num,
        'base_filename': base_filename,
        'library': library,
        'algorithm': "MLP",
        'config': MLP_config,
        'train_time': str(MLP_train_time),
        'predict_time': str(MLP_predict_time),
        'true_positives': str(MLP_tp),
        'true_negatives': str(MLP_tn),
        'false_positives': str(MLP_fp),
        'false_negatives': str(MLP_fn),
        'notes': MLP_notes
    }

    KNN_fields = {
        'test_set': test_set,
        'test_number': test_num,
        'base_filename': base_filename,
        'library': library,
        'algorithm': "KNN",
        'config': KNN_config,
        'train_time': str(KNN_train_time),
        'predict_time': str(KNN_predict_time),
        'true_positives': str(KNN_tp),
        'true_negatives': str(KNN_tn),
        'false_positives': str(KNN_fp),
        'false_negatives': str(KNN_fn),
        'notes': KNN_notes
    }

    SVM_fields = {
        'test_set': test_set,
        'test_number': test_num,
        'base_filename': base_filename,
        'library': library,
        'algorithm': "SVM",
        'config': SVM_config,
        'train_time': str(SVM_train_time),
        'predict_time': str(SVM_predict_time),
        'true_positives': str(SVM_tp),
        'true_negatives': str(SVM_tn),
        'false_positives': str(SVM_fp),
        'false_negatives': str(SVM_fn),
        'notes': SVM_notes
    }

    insert_statement= ("INSERT INTO test_results "
                       "(test_set, test_number, base_filename, library, algorithm, config, train_time, predict_time, "
                       "true_positives, true_negatives, false_positives, false_negatives, notes) VALUES "
                       "(%(test_set)s, %(test_number)s, %(base_filename)s, %(library)s, %(algorithm)s, %(config)s, "
                       "%(train_time)s, %(predict_time)s, %(true_positives)s, %(true_negatives)s, "
                       "%(false_positives)s, %(false_negatives)s, %(notes)s)")

    #create connection and cursor
    connection = mysql.connect(user='root', password='', host='127.0.0.1', database='mltests')
    cursor = connection.cursor()

    #execute commands
    cursor.execute(insert_statement, MLP_fields)
    cursor.execute(insert_statement, KNN_fields)
    cursor.execute(insert_statement, SVM_fields)

    #commit changes
    connection.commit()

    #close connection
    cursor.close()
    connection.close()

    print("Multilayer Perceptron")
    print("Accuracy Score: ", metrics.accuracy_score(y_test, y_pred_MLP))
    print("Train Time: " , str(MLP_train_time))
    print("Predict Time: " , str(MLP_predict_time))
    print("Confusion Matrix: \n", metrics.confusion_matrix(y_test, y_pred_MLP))
    print()
    print("K-Nearest Neighbour")
    print("Accuracy Score: ", metrics.accuracy_score(y_test, y_pred_KNN))
    print("Train Time: " , str(KNN_train_time))
    print("Predict Time: " , str(KNN_predict_time))
    print("Confusion Matrix: \n", metrics.confusion_matrix(y_test, y_pred_KNN))
    print()
    print("Support Vector Machine")
    print("Accuracy Score: ", metrics.accuracy_score(y_test, y_pred_SVM))
    print("Train Time: " , str(SVM_train_time))
    print("Predict Time: " , str(SVM_predict_time))
    print("Confusion Matrix: \n", metrics.confusion_matrix(y_test, y_pred_SVM))


